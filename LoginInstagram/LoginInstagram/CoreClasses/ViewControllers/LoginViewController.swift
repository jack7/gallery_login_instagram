//
//  LoginViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import TOWebViewController
import Alamofire


struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "..."
    
    static let INSTAGRAM_CLIENTSERCRET = "..."
    
    static let INSTAGRAM_REDIRECT_URI = "http://localhost/"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

class LoginViewController: BaseViewController {

    @IBOutlet weak var btnLogin: UIButton!
    
    var webView : TOWebViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Utility.transparentNavBar(controller: self.navigationController!)
      

        self.setDefualt()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.clearSafariCache()

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - HelperMethods
    
    func setDefualt(){
        
        self.btnLogin.layer.cornerRadius = 7.0
    }

    func clearSafariCache()  {
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
    }
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        
        
        self.performSegue(withIdentifier: "LoginNavWebViewController", sender: self)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        print(requestURLString)
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            
            self.webView.dismiss(animated: true, completion: nil)
            
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        
        print("Instagram authentication token ==", authToken)
        
        let url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + authToken
        
        
        Alamofire.request(url, method: .get, parameters: nil)
            .responseJSON { response in
                
                if response.result.isSuccess{
                    
                    if let value = response.result.value {
                        
                        print("Value is === \(value)")
                    }
                }
        }
    }

    
  
}
