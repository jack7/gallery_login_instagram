//
//  HomeCollectionReusableView.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import SDWebImage

class HomeCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var viewPhotos: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNumberOfPosts: UILabel!
    @IBOutlet weak var lblNumberOfFollowers: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func awakeFromNib() {
        
        self.setDefault()
    }
    
    override func prepareForReuse() {
        
        self.setDefault()
    }
    
    func setDefault(){
        
        self.lblUserName.text = ""
        
        self.lblNumberOfFollowers.text = ""
        
        self.lblNumberOfPosts.text = ""
        
        self.imgProfile.image = PROFILE_PLACEHOLDER
        
        
    }
    
    func getUserMediaCounts(dict : [String : AnyObject]?){
        
        if  dict != nil {
            
            if let posts = dict?["media"] as? Int{
                
                self.lblNumberOfPosts.text = String(describing: posts)
            }
            
            if let followers = dict?["followed_by"] as? Int{
                
                self.lblNumberOfFollowers.text = String(describing: followers)
            }
        }
    }
    
    func populateHeader(dict : [String : AnyObject]?){
        
        if dict != nil{
        
            self.lblUserName.text = dict?["username"] as! String?
        
            self.getUserMediaCounts(dict: dict?["counts"] as? [String : AnyObject])
        
            self.activityIndicator.startAnimating()
        
            let url = URL(string: dict?["profile_picture"] as! String)
        
            self.imgProfile.sd_setImage(with: url, placeholderImage: PROFILE_PLACEHOLDER, options:[] ,completed: { (img, _, _, _) in
            
                if (img != nil) {
                
                    DispatchQueue.main.async {
                    
                        self.imgProfile.image = img
                    
                        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2.0
                    
                        self.imgProfile.clipsToBounds = true
                    
                    }
                
                
                }
                self.activityIndicator.stopAnimating()
            })
        }
    }
}
