//
//  WelcomeViewController.swift
//  LoginInstagram
//
//  Created by GiaBoemio on 11/08/2017.
//  Copyright © 2017 Jack7. All rights reserved.
//

import UIKit
import Alamofire

class WelcomeViewController: BaseViewController {
    
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    var userDictionary = [String : AnyObject]()

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        Utility.transparentNavBar(controller: self.navigationController!)
    
        self.setDefault()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - helperMethods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "HomeViewController"{
         
            let vc = segue.destination as! HomeViewController
            
            if let value = sender as? [String : AnyObject]{
                
                vc.userDictionary = value
            }
            
            
            
        }
    }
    
    func setDefault() {
        
        self.lblName.text = "Welcome"
        self.btnProfile.layer.cornerRadius = 7.0

        
        self.callServiceOfUser()
       
    }
    
    func showNoDataLabel(){
     
        self.lblName.isHidden = true
        
        self.btnProfile.isHidden = true
    
        self.lblNoData.isHidden = false
    }
    
    func hideNoDataLabel(){
        
        self.lblName.isHidden = false
        
        self.btnProfile.isHidden = false
        
        self.lblNoData.isHidden = true
    }
    
    func callServiceOfUser(){
        
        let user = User.loadUser()
        
        let url = "https://api.instagram.com/v1/users/self/?access_token=" + (user?.accessToken)!
        
        self.showActivityIndicator()
        
        print(url)
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { response in
            
           
            
            if response.result.isSuccess{
                
                self.hideActivityIndicator()
                
                if let value = response.result.value {
                
                    if let tempValue = value as? [String : AnyObject]{
                        
                        if let meta = tempValue["meta"] as? [String : AnyObject]{
                            
                            if  let code = meta["code"] as? Int {
                                
                                if code == 200{
                                    
                                    self.hideNoDataLabel()
                                    
                                    self.userDictionary = [String : AnyObject]()
                                    
                                    let tempValue = (value as? [String : AnyObject])!
                                    
                                    self.userDictionary = tempValue["data"] as! [String : AnyObject]!
                                    
                                    if let name = self.userDictionary["full_name"] as? String{
                                        
                                        self.lblName.text = NSLocalizedString("WelcomeNameOfPerson", comment: "WelcomeWithThePersonName") + name
                                        
                                        let user = User.loadUser()
                                        
                                        user?.name = name
                                        
                                        user?.saveUser()
                                        
                                    }
                                    
                                    
                                    
                                    print("User is :\(self.userDictionary)")
                                    
                                }
                                else{
                                    
                                    self.showNoDataLabel()
                                    
                                    self.hideActivityIndicator()
                                    
                                    self.showErrorAlertWithMessage(message: "Server Error")
                                }
                            }
                        }
                    }
                    
                    
                    
                }else{
                    
                    self.showNoDataLabel()
                    
                    self.hideActivityIndicator()
                    
                    self.showErrorAlertWithMessage(message: "Server Error")
                }
                
            }
            
        }
        
    }


     // MARK: - ActionMethods
    @IBAction func btnProfilePressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "HomeViewController", sender: self.userDictionary)
    }
}
